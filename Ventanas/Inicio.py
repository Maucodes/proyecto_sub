#coding: utf-8
#author: mau
from kivy.uix.screenmanager import Screen
import sys

class Inicio(Screen):
    def __init__(self,**kargs):
        Screen.__init__(self,**kargs)
        self.name = "inicio"


    def salir(self):
        sys.exit(0)

    def ingresar(self):
        self.manager.current = "camara"