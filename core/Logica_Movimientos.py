#author: mau

class Logica_Movimientos():

    def __init__(self,creatura):
        self.creatura = creatura
        self.objetivo_a_caminar = [0.0,0.0]
        self.en_movimiento = False


    def objetivo_a_buscar(self,creatura):
        posiciones = [self.creatura.get_x(),self.creatura.get_y()]
        posicion_creatura = [creatura[0],creatura[1]]
        resultado_distancia = [posicion_creatura[0]-posiciones[0],posicion_creatura[1]-posiciones[1]]
        self.objetivo_a_caminar = resultado_distancia
        self.en_movimiento = True

    def actualizar(self,dt):
        if self.objetivo_a_caminar[0] <= self.creatura.get_x() or self.objetivo_a_caminar[1] <= self.creatura.get_y():
            self.en_movimiento = False
        else:
            self.creatura.set_x(self.creatura.get_x()+1)
            self.creatura.set_y(self.creatura.get_y() + 1)
            self.creatura.pos = [self.creatura.get_x(),self.creatura.get_y()]
            self.en_movimiento = True


    def movimiento_objetivo(self,touch):
        pass