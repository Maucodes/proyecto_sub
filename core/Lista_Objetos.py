# coding: utf-8
# author: mau

from Entidad.Objetos import Objetos
import copy


class Lista_Objetos():

    def __init__(self):
        self.lista_sprites = {1:"atlas://assets/Escenario/Biomas/bioma_agua/",
                              2: "atlas://assets/Escenario/Biomas/bioma_verde/",
                              3: "atlas://assets/Escenario/Biomas/bioma_tierra/",
                              4: "atlas://assets/Escenario/Biomas/bioma_desertico/",
                              }

        self.__lista_objetos = {1: Objetos(self.lista_sprites[1], "suelo",animaciones={"sur":[1]}),
                                2: Objetos(self.lista_sprites[1], "suelo",animaciones={"sur":[2,]}),
                                3: Objetos(self.lista_sprites[1], "suelo",animaciones={"sur":[1,]}),
                                4: Objetos(self.lista_sprites[1], "suelo",animaciones={"sur":[4,]}),
                                5: Objetos(self.lista_sprites[1], "suelo",animaciones={"sur":[5,]})}
        self.__lista_vestimentas = {}

    def pedir_objeto(self,id_objeto):
        return copy.copy(self.__lista_objetos[id_objeto])

