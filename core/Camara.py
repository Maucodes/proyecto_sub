#coding: utf-8
#author: mau
from kivy.uix.screenmanager import Screen
from kivy.uix.floatlayout import FloatLayout

from Entidad.Personaje import Personaje
from core.Lista_Objetos import Lista_Objetos
from core.Logica_Movimientos import Logica_Movimientos
from mapa.Mapa import Mapa

import math
class Camara(Screen):
    def __init__(self,variables,**kargs):
        Screen.__init__(self,**kargs)
        self.variables = variables
        self.configuracion_relacion_aspecto()
        self.name = "camara"
        self.pantalla = FloatLayout(size = self.size)
        self.mouse_detectar = [0,0]
        self.add_widget(self.pantalla)
        self.lista_obj = Lista_Objetos()
        self.mapa = Mapa(10,10,10)

        self.control_de_objetos_en_pantalla_valga_la_redundancia_echa_por_el_juego_que_estamos_creando = []

        self.jugador = Personaje(ruta= "atlas://assets/Personajes/Male/Male 01-4/",nombre = "Wayatt",x= 0,y= 0,z= 0,vida = 0,mana = 0,experiencia = 10,**kargs)
        self.logica_movimiento = Logica_Movimientos(self.jugador)


        with self.pantalla.canvas.before:
            self.pantalla.canvas.add(self.jugador)



    def generar_mapa(self):
        self.pantalla.canvas.clear()
        self.control_de_objetos_en_pantalla_valga_la_redundancia_echa_por_el_juego_que_estamos_creando.clear()
        for x in range(len(self.mapa.mapa_pre_Echo)):
            for y in range(len(self.mapa.mapa_pre_Echo[x])):
                if self.mapa.get_pos(x,y) is None:
                    pass
                else:
                    self.jugador.dibujar([0, 0], self.variables["animacion_concurrente"])
                    obj = self.lista_obj.pedir_objeto(self.mapa.get_pos(x,y))
                    obj.set_x(x)
                    obj.set_y(y)
                    self.pantalla.canvas.add(obj)
                    self.pantalla.canvas.add(self.jugador)
                    obj.dibujar([x*self.cuadros_x,y*self.cuadros_y],self.variables["animacion_concurrente"])
                    self.control_de_objetos_en_pantalla_valga_la_redundancia_echa_por_el_juego_que_estamos_creando.append(obj)


    def configuracion_relacion_aspecto(self):
        self.cuadros_x = self.calcular_primos(math.ceil(self.variables["tamanio"][0]/self.variables["relacion_vista"]["x"]))
        self.cuadros_y = self.calcular_primos(math.ceil(self.variables["tamanio"][1]/self.variables["relacion_vista"]["y"]))

    def calcular_primos(self,valor):
        if valor%2==0:
            return valor
        else:
            while valor%2!=0: #Repetir hasta que sea igual 0
                valor+=1
                print(valor)
            return valor

    def on_touch_down(self, touch):
        self.mouse_detectar = list(touch.pos)
        self.mouse_detectar = [self.mouse_detectar[0] / self.cuadros_x,self.mouse_detectar[1] / self.cuadros_y]
        self.jugador.en_movimiento = True
        self.logica_movimiento.objetivo_a_buscar(list(touch.pos))


    def on_touch_up(self,touch):
        pass
        #self.jugador.en_movimiento =False

    def on_touch_move(self, touch):
        pass


    def actualizar(self,*dt):
        self.configuracion_relacion_aspecto()
        self.logica_movimiento.actualizar(dt)
        self.jugador.actualizar([self.cuadros_x,self.cuadros_y])

    def dibujar(self,*dt):
        self.generar_mapa()



