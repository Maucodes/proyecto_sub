#coding: utf-8
#author: mau
import kivy
from kivy.core.window import Window

from core.Camara import Camara

kivy.require("1.11.1")
from kivy.app import App
from kivy.uix.screenmanager import ScreenManager
from kivy.clock import Clock


#clases del jeugo o ventanas
from Ventanas.Inicio import Inicio



VARIABLES ={"animacion_concurrente":0,
            "titulo": "la cruzada de brosito",
            "tamanio":[1,1],
            "relacion_vista":{"x":14,"y":8}}


class Juego(App):

    def __init__(self,**kargs):
        App.__init__(self,**kargs)
        self.title = VARIABLES["titulo"]


    def animar(self):
        VARIABLES["animacion_concurrente"] +=1
        if VARIABLES["animacion_concurrente"] >= 37455:
            VARIABLES["animacion_concurrente"] = 0

    def build(self):
        self.control_ventanas = ScreenManager()
        self.inicio = Inicio()
        self.camara = Camara(VARIABLES)
        self.control_ventanas.add_widget(self.inicio)
        self.control_ventanas.add_widget(self.camara)
        Clock.schedule_interval(self.dibujar,1/60)
        Clock.schedule_interval(self.actualizar,1/120)
        return self.control_ventanas

    def actualizar(self,*dt):
        VARIABLES["tamanio"] = list(Window.size)
        self.camara.actualizar(dt)

    def dibujar(self,*dt):
        self.animar()
        self.camara.dibujar(dt)


if __name__ == "__main__":
    app = Juego()
    app.run()