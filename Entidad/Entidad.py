#coding utf-8
#author mau
from kivy.graphics.vertex_instructions import Rectangle


class Entidad(Rectangle):
    def __init__(self,ruta,nombre,x,y,z,vida,mana,experiencia,animaciones,**kargs):
        Rectangle.__init__(self,**kargs)
        self._nombre = nombre
        self.tipo = ""
        self._pos={"x":x,"y":y,"z":z}
        self.vida = vida
        self.vida_max = vida
        self.mana = mana
        self.mana_max = mana
        self.experiencia = experiencia
        self.__destruible = False
        self.atlas = ruta
        self.animacion = animaciones
        self.estado_animacion = 1
        self.en_movimiento = False
        self.en_direccion = "sur"

    def actualizar(self,relacion_aspecto,*dt):
        self.size = relacion_aspecto

    def dibujar(self,pos,*dt):
        if len(self.atlas) >= 2:
            self.pos = pos
            if self.en_movimiento:
                self.source = self.atlas+str(self.estado_animacion)
            else:
                if self.tipo == "vestimenta":
                    self.source = self.atlas+str(self.animacion[self.en_direccion][1])

                elif self.tipo == "objetos":
                    self.source = self.atlas + str(self.animacion[self.en_direccion][0])


    def set_destruible(self,valor):
        self.__destruible = valor

    def set_x(self,x):
        self._pos["x"] = x

    def set_y(self,y):
        self._pos["y"] = y

    def set_z(self,z):
        self._pos["z"] = z

    def get_x(self):
        return self._pos["x"]

    def get_y(self):
        return self._pos["y"]

    def get_z(self):
        return self._pos["z"]

    def set_pos(self,pos):
        if isinstance(pos, tuple):
            self.__set_pos_encapsulado({"x":pos[0],"y":pos[1],"z":pos[2]})
        elif isinstance(pos,list):
            self.__set_pos_encapsulado({"x":pos[0],"y":pos[1],"z":pos[2]})
        elif isinstance(pos,dict):
            self.__set_pos_encapsulado(pos)
        else:
            print("No se puede setera la posicion hay")

    def __set_pos_encapsulado(self,valores):
        self._pos = valores
