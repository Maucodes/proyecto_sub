#coding: utf-8
#author: mau
from Entidad.Entidad import Entidad


class Personaje(Entidad):
    def __init__(self,ruta,nombre,x= 0,y= 0,z= 0,vida = 0,mana = 0,experiencia = 0,**kargs):
        Entidad.__init__(self,ruta,nombre,x,y,z,vida,mana,experiencia ,
                         animaciones = {"norte":[1,5,9],
                          "sur":[4,8,12],
                          "izquierda":[3,7,11],
                          "derecha":[2,6,10],
                          "ataque_norte":[1,9],
                          "ataque_sur":[4,12],
                          "ataque_izquierda":[3,11],
                          "ataque_derecha":[2,10]},**kargs)
        self.tipo = "vestimenta"
        self.nivel = 1

    def movimiento_sur(self,*dt):
        self.en_direccion = "sur"

    def movimiento_norte(self,*dt):
        self.en_direccion = "norte"

    def movimiento_derecha(self,*dt):
        self.en_direccion = "derecha"

    def movimiento_izquierda(self,*dt):
        self.en_direccion = "izquierda"

    def actualizar(self,relacion_aspecto,*dt):
        super().actualizar(relacion_aspecto)

    def dibujar(self,pos,animacion_concurrente,*dt):
        super().dibujar(pos,dt)
        if animacion_concurrente % 20 >= 10:
            self.estado_animacion = self.animacion[self.en_direccion][0]
        elif animacion_concurrente % 20 <= 5:
            self.estado_animacion = self.animacion[self.en_direccion][2]
