#coding: utf-8
#author: mau
from Entidad.Entidad import Entidad
class Objetos(Entidad):

    def __init__(self, ruta, nombre, x= 0, y= 0, z= 0, vida = 0, mana = 0, experiencia = 0, animaciones=None, **kargs):
        Entidad.__init__(self,ruta,nombre,x,y,z,vida,mana,experiencia,
                         animaciones,**kargs)
        if animaciones is None:
            self.animacion = {"sur": [1, ]}
        self.tipo = "objetos"


    def actualizar(self,relacion_aspecto,*dt):
        super().actualizar(relacion_aspecto)

    def dibujar(self,pos,animacion_concurrente,*dt):
        super().dibujar(pos,*dt)
        if len(self.animacion["sur"]) >= 2:
            if animacion_concurrente % 20 >= 10:
                self.estado_animacion = self.animacion[self.en_direccion][0]
            elif animacion_concurrente % 20 <= 5:
                self.estado_animacion = self.animacion[self.en_direccion][1]