#Coding:utf-8
#author: mau

class Mapa():

    def __init__(self,x,y,z):
        self.mapa = self.iniciar_mapa(x,y,z)
        self.mapa_pre_Echo = [[2, 2, 2, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 2, 2, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1],
                              [2, 2, 2, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 2, 2, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1],
                              [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 2, 2, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1],
                              [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 2, 2, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1],
                              [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 2, 2, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1],
                              [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 2, 2, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1],
                              [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 2, 2, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1],
                              [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 2, 2, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1]
                              ]

    def get_pos(self,x,y):
        return self.mapa_pre_Echo[x][y]

    def set_pos(self,x,y,valor):
        self.mapa_pre_Echo[x][y] = valor


    def iniciar_mapa(self,x,y,z):
        return [[[None for columna in range(x)] for columna in range(y)] for fila in range(z)]


    def insertar_objeto(self,id,pos):
        if len(self.mapa) >= 2:
            self.mapa[pos[0]][pos[1]][pos[2]] = id

    def obtener_objeto(self,pos):
        if len(self.mapa) >= 2:
            return self.mapa[pos[0]][pos[1]][pos[2]]
        else:
            print("no se puede bsucar objetos en una lista basa")
            return None
